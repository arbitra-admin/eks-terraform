variable "region" {
  default = "ap-southeast-2"
}

variable "cluster_name" {
	default = "demo-terraform"
}
