region="ap-southeast-2"
cluster_name="eks-demo"
vpc_name="eks-demo-vpc"
fargate_profile_name="default"
map_users=[
	{
		userarn  = "arn:aws:iam::1234567890:user/eks-admin-nonprod"
		username = "eks-admin-nonprod"
		groups   = ["eks-nonprod-admins"]
	}
]