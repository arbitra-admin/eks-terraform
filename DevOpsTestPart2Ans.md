Please describe how you would implement the following requirements:
- How would you provide shell access into the application stack for operations staff who may want to log into an instance.

ANSWER: For this microservices implementation, the operationns staff has to firstly authenticate in AWS ECR where latest docker build is located by executing the following commands:
```
aws ecr get-login-password --region ap-southeast-2 | docker login --username AWS --password-stdin 12345678901.dkr.ecr.ap-southeast-2.amazonaws.com

docker pull 12345678901.dkr.ecr.ap-southeast-2.amazonaws.com/<continer_name>
or
docker pull 12345678901.dkr.ecr.ap-southeast-2.amazonaws.com/<continer_name>:latest
```

- Make access and error logs available in CloudWatch Logs
ANSWER:  For EKS Implementation, include the snippet below as part of aws_eks_cluster resource.

```
variable "cluster_name" {
  default = "demo-terraform"
  type    = "string"
}

resource "aws_eks_cluster" "cluster" {
  depends_on = ["aws_cloudwatch_log_group.example"]

  enabled_cluster_log_types = ["api", "audit"]
  name                      = "${var.cluster_name}"

  # ... other configuration ...
}

resource "aws_cloudwatch_log_group" "eks_cluster_loggroup" {
  name              = "/aws/eks/${var.cluster_name}/cluster"
  retention_in_days = 7

  # ... potentially other configuration ...
}
```