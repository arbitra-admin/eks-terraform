# Why DevOpsTest requirement is not only the pattern

# Please clone the repository below:
- git clone https://arbitra-admin@bitbucket.org/arbitra-admin/terraform-node-frontend.git

# PURPOSE #
This is a comperehensive guide on how to standardise the deployment of Front End resources written in Javascript which uses AWS Resources such as S3 Bucket, CloudFront Distribution, Route53 and AWS Certificate Manager. The main differentiator of this deployment boilerplate is the Infrastructure As Code component that will drive value on code being repeatable, auditable and efficient.

# APPROACH #
The deployment of both Application and Infrastructure will be driven by Terraform.